FROM ubuntu:16.04

ENV DEBIAN_FRONTEND noninteractive

SHELL ["/bin/bash", "--login", "-c"]

WORKDIR /

RUN apt-get update -y && \
    apt-get dist-upgrade -y && \
    apt-get install -y \
      gawk zlib1g-dev libyaml-dev libsqlite3-dev sqlite3 autoconf libgmp-dev \
      libgdbm-dev libncurses5-dev automake libtool bison pkg-config libffi-dev \
      libgmp-dev libreadline6-dev libssl-dev \
      sudo build-essential curl jq tzdata git-core apt-transport-https \
    && \
    gpg --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3 7D2BAF1CF37B13E2069D6956105BD0E739499BDB && \
    curl -sSL https://get.rvm.io | bash -s stable --auto-dotfiles && \
    source /etc/profile.d/rvm.sh && \
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add - && \
    echo 'deb [arch=amd64] https://download.docker.com/linux/ubuntu xenial stable' >/etc/apt/sources.list.d/docker.list && \
    apt-get update -y && \
    apt-get install -y docker-ce && \
    apt-get autoremove --purge -y && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* /root/.gem/

CMD []